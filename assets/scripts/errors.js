export default {
  email: 'Неверный логин',
  password: 'Неверный пароль',
  small_password: 'Пароль должен быть больше 6 символов',
  name: 'Имя слишком короткое',
  required: 'Это обязательное поле',
  'auth/user-not-found': 'Пользователь не найден',
  'auth/wrong-password': 'Неверный пароль',
  'auth/email-already-in-use': 'Такой логин уже занят',
};
